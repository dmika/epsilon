///
/// \file epsilon_esp32.ino
/// \brief Wi-Fi version of the "epsilon" device.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2024, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of epsilon.
/// epsilon is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// epsilon is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with epsilon. If not, see <https://www.gnu.org/licenses/>.
///


#include <WiFi.h>
#include <WiFiClient.h>
#include <WiFiAP.h>


unsigned int TcpPort = 51234;
WiFiServer  TcpServer(TcpPort);
unsigned char ReadBuffer[16];
unsigned TimePowerOn = 0;
unsigned SampleSize = 262144;
unsigned SampleSizeFft = 4096;


struct ReadCommand
{
    uint32_t type;
    float measFreq;
    float samplFreq;
    float amplitude;
};


void setup()
{
    initPower();
    initSpi();
    initAD9958();
    initFifo();
    initMeasure();
    initWiFi("epsilon","");
    delay(100);
}


void loop()
{
    if (TcpServer.hasClient()) {
        TimePowerOn = 0;
        WiFiClient TcpClient = TcpServer.available();
        while (TcpClient && TcpClient.connected()) {
            if (TcpClient.available()) {
                ReadCommand command;
                TcpClient.read((uint8_t*)&command, 16);
                uint16_t *waveData = handleCommand(command);
                TcpClient.write((uint8_t*)waveData, SampleSize*2*2);
            }
        }
    }
    else {
        delay(1);
        TimePowerOn++;
        if (TimePowerOn > 900000) { // 15 minutes
            turnOffDevice();
        }
    }
}


void initWiFi(char* name, char* password)
{
    WiFi.disconnect();
    WiFi.mode(WIFI_AP_STA); // access point
    WiFi.softAPConfig(IPAddress(192, 168, 10, 1), IPAddress(192, 168, 10, 1), IPAddress(255, 255, 255, 0)); // ip, gateway, subnet
    WiFi.softAP(name, password);
    delay(50);
    TcpServer.begin();
}
