///
/// \file ad9958.ino
/// \brief Controlling AD9958.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2024, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of epsilon.
/// epsilon is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// epsilon is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with epsilon. If not, see <https://www.gnu.org/licenses/>.
///


#include <SPI.h>


#define UPDATE_AD9958 22
#define RESET_AD9958  21
#define SCLK_VSPI     18
#define MOSI_VSPI     23

#define CSR_ADDRESS   0x00
#define FR1_ADDRESS   0x01
#define FR2_ADDRESS   0x02
#define CFR_ADDRESS   0x03
#define CFTW0_ADDRESS 0x04
#define CPOW0_ADDRESS 0x05
#define ACR_ADDRESS   0x06


SPIClass *ESP32_VSPI = NULL;


void initSpi(void)
{
    ESP32_VSPI = new SPIClass(VSPI);
    ESP32_VSPI->begin(SCLK_VSPI, MOSI_VSPI);
}


void initAD9958(void)
{
    pinMode(UPDATE_AD9958, OUTPUT);
    pinMode(RESET_AD9958, OUTPUT);

    digitalWrite(UPDATE_AD9958, LOW);
    digitalWrite(RESET_AD9958, LOW);

    resetAD9958();

    uint8_t data[] = {FR1_ADDRESS, 0b11010000, 0b00000000, 0b00000000};
    sendCommandToAD9958(data, 4);

    updateAD9958();
}


void selectChannelAD9958(uint8_t channel)
{
    uint8_t data[] = {CSR_ADDRESS, ((1<<6) << channel) & 0xFF};
    sendCommandToAD9958(data, 2);
}


void setFrequencyAD9958(uint8_t channel, float frequency)
{
    selectChannelAD9958(channel);
    uint32_t cftw = (uint32_t)(frequency * 4294967296.0 / 500.0e6 + 0.5);
    setFtwAD9958(cftw);
}


void setFtwAD9958(uint32_t ftw)
{
    uint8_t data[] = {CFTW0_ADDRESS, (ftw>>24)&0xFF, (ftw>>16)&0xFF, (ftw>>8)&0xFF, ftw&0xFF};
    sendCommandToAD9958(data, 5);
    updateAD9958();
}


void setPhaseAD9958(uint8_t channel, float phase)
{
    selectChannelAD9958(channel);
    uint16_t cpow = (uint16_t)(phase * 16384.0 / 360.0 + 0.5);
    setPowAD9958(cpow);
}


void setPowAD9958(uint16_t pow)
{
    uint8_t data[] = {CPOW0_ADDRESS, (pow>>8)&0xFF, pow&0xFF};
    sendCommandToAD9958(data, 3);
    updateAD9958();
}


void setAmplitudeAD9958(uint8_t channel, float amplitude)
{
    selectChannelAD9958(channel);
    if (amplitude > 1.0) {
        amplitude = 1.0;
    }
    if (amplitude < 0.0) {
        amplitude = 0.0;
    }
    uint16_t asf = (uint16_t)(amplitude * 1023.0 + 0.5);
    setAsfAD9958(asf);
}


void setAsfAD9958(uint16_t asf)
{
    uint8_t data[] = {ACR_ADDRESS, 0, 0b00010000 | (asf>>8)&0x03, asf&0xFF};
    sendCommandToAD9958(data, 4);
    updateAD9958();
}


void sendCommandToAD9958(uint8_t data[], unsigned size)
{
    int spiFreq = 1000000;
    ESP32_VSPI->beginTransaction(SPISettings(spiFreq, MSBFIRST, SPI_MODE0));
    for (unsigned cnt = 0; cnt < size; ++cnt) {
        ESP32_VSPI->transfer(data[cnt]);
    }
    ESP32_VSPI->endTransaction();
}


void resetAD9958(void)
{
    delay(1);
    digitalWrite(RESET_AD9958, HIGH);
    delay(1);
    digitalWrite(RESET_AD9958, LOW);
    delay(1);
}


void updateAD9958(void)
{
    delay(1);
    digitalWrite(UPDATE_AD9958, HIGH);
    delay(1);
    digitalWrite(UPDATE_AD9958, LOW);
    delay(1);
}
