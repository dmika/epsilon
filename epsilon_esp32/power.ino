///
/// \file power.ino
/// \brief Controlling the power sources.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2024, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of epsilon.
/// epsilon is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// epsilon is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with epsilon. If not, see <https://www.gnu.org/licenses/>.
///


#define POWER_GEN15V      2
#define POWER_3V5V        4
#define POWER_OFF_DEVICE  5


void initPower(void)
{
    pinMode(POWER_3V5V, OUTPUT);
    pinMode(POWER_GEN15V, OUTPUT);
    pinMode(POWER_OFF_DEVICE, OUTPUT);
    digitalWrite(POWER_3V5V, LOW);
    digitalWrite(POWER_GEN15V, LOW);
    digitalWrite(POWER_OFF_DEVICE, HIGH);
}


void turnOnPowerForCalibration(void)
{
    digitalWrite(POWER_3V5V, HIGH);
    delay(300);
}


void turnOffPowerForCalibration(void)
{
    digitalWrite(POWER_3V5V, LOW);
    delay(300);
}


void turnOnPowerForGenerator(void)
{
    digitalWrite(POWER_GEN15V, HIGH);
    delay(1000);
}


void turnOffPowerForGenerator(void)
{
    digitalWrite(POWER_GEN15V, LOW);
    delay(1000);
}


void turnOffDevice(void)
{
    digitalWrite(POWER_OFF_DEVICE, LOW);
}
