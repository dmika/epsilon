///
/// \file measure.ino
/// \brief Measuring the signal.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2024, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of epsilon.
/// epsilon is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// epsilon is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with epsilon. If not, see <https://www.gnu.org/licenses/>.
///


#include "esp_dsp.h"


#define TURN_ON_3V5V    0b00000001
#define TURN_OFF_3V5V   0b00000010
#define TURN_ON_GEN15V  0b00000100
#define TURN_OFF_GEN15V 0b00001000
#define CALC_REF_AMPL   0b01000000
#define ADJUST_AMPL     0b10000000


extern unsigned SampleSize;
extern unsigned SampleSizeFft;
uint16_t *WaveData;
float *FftData;
float RefAmplitude;


void initMeasure(void)
{
    WaveData = (uint16_t*)heap_caps_malloc(SampleSize*sizeof(uint16_t)*2, MALLOC_CAP_SPIRAM);
    FftData = (float*)heap_caps_malloc(SampleSizeFft*sizeof(float)*2, MALLOC_CAP_SPIRAM);
}


uint16_t* handleCommand(ReadCommand command)
{
    if (command.type & TURN_ON_GEN15V) {
        turnOnPowerForGenerator();
    }
    if (command.type & TURN_ON_3V5V) {
        turnOnPowerForCalibration();
    }

    if (command.type & CALC_REF_AMPL) {
        ReadCommand tmpcmd = command;
        tmpcmd.amplitude = 0.15;
        tmpcmd.measFreq = 10.0117177;
        unsigned frequencyNumber = calcFreqNumber(&tmpcmd);
        RefAmplitude = calcSignalAmplitude(tmpcmd, frequencyNumber);
        adjustSignalAmplitude(&command);
    }
    else if (command.type & ADJUST_AMPL && command.measFreq > 30.0) {
        adjustSignalAmplitude(&command);
    }

    command.type &= ~CALC_REF_AMPL;
    command.type &= ~ADJUST_AMPL;
    measureSineWave(command);

    if (command.type & TURN_OFF_GEN15V) {
        turnOffPowerForGenerator();
    }
    if (command.type & TURN_OFF_3V5V) {
        turnOffPowerForCalibration();
    }

    return WaveData;
}


float calcSignalAmplitude(ReadCommand command, unsigned freqNum)
{
    measureSineWave(command);

    esp_err_t ret = dsps_fft2r_init_fc32(NULL, CONFIG_DSP_MAX_FFT_SIZE);
    for (unsigned cnt = 0; cnt < SampleSizeFft; ++cnt) {
        FftData[cnt*2] = WaveData[cnt];
        FftData[cnt*2+1] = 0.0;
    }
    dsps_fft2r_fc32(FftData, SampleSizeFft);
    dsps_bit_rev2r_fc32(FftData, SampleSizeFft);
    float re = FftData[freqNum*2];
    float im = FftData[freqNum*2+1];

    return sqrt(re*re + im*im);
}


void adjustSignalAmplitude(ReadCommand* command)
{
    ReadCommand tmpcmd = *command;
    tmpcmd.amplitude = 0.15;
    unsigned frequencyNumber = calcFreqNumber(&tmpcmd);
    float ampl = calcSignalAmplitude(tmpcmd, frequencyNumber);
    if (ampl > 0.0) {
        float adjCoef = RefAmplitude/ampl;
        command->amplitude *= adjCoef;
    }
}


unsigned calcFreqNumber(ReadCommand* command)
{
    float tempSampFreq;
    if (command->measFreq < 30.0) {
        float numberOfPeriods = 5.0;
        tempSampFreq = command->measFreq*SampleSizeFft/numberOfPeriods;
    }
    else {
        float pointsPerPeriod = 20;
        tempSampFreq = command->measFreq*pointsPerPeriod;
    }
    float df = tempSampFreq/SampleSizeFft;
    unsigned frequencyNumber = (unsigned)(command->measFreq / df + 0.5);
    float dfReal =  command->measFreq / (float)frequencyNumber;
    command->samplFreq = dfReal * SampleSizeFft;
    return frequencyNumber;
}


void measureSineWave(ReadCommand command)
{
    float measPhase = 90.0;
    if (command.measFreq < 1.0e-3) {
        command.measFreq = 0.0;
        measPhase = 0.0;
        if (command.amplitude < 0.0) {
            measPhase = 180.0;
            command.amplitude = -command.amplitude;
        }
    }

    initAD9958();
    setAmplitudeAD9958(0, command.amplitude);
    setFrequencyAD9958(0, command.measFreq);
    setPhaseAD9958(0, measPhase);
    setAmplitudeAD9958(1, 1);
    setFrequencyAD9958(1, command.samplFreq);

    if (command.type & CALC_REF_AMPL || command.type & ADJUST_AMPL) {
        unsigned delaytime = (unsigned)(7.0+SampleSizeFft*1.0e3/command.samplFreq);
        measureCurrent(WaveData, delaytime);
    }
    else {
        unsigned delaytime = (unsigned)(7.0+(float)SampleSize*1.0e3/command.samplFreq);
        measureCurrentAndVoltage(WaveData, WaveData+SampleSize, delaytime);
    }
}
