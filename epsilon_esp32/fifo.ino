///
/// \file fifo.ino
/// \brief Controlling the FIFO memory.
/// \author Dmitry Kuklin <vkd@tuta.io>
/// \copyright 2024, Dmitry Kuklin
/// \license GPLv3
///
/// \section LICENSE
/// This file is part of epsilon.
/// epsilon is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License version 3
/// as published by the Free Software Foundation.
/// epsilon is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU General Public License for more details.
/// You should have received a copy of the GNU General Public License
/// along with epsilon. If not, see <https://www.gnu.org/licenses/>.
///


#define CHOOSE_CHANNEL 13
#define RCLK           15
#define WEN            19
#define RESET_FIFO     5
#define FIFO_BIT0      36
#define FIFO_BIT1      37
#define FIFO_BIT2      38
#define FIFO_BIT3      39
#define FIFO_BIT4      34
#define FIFO_BIT5      35
#define FIFO_BIT6      32
#define FIFO_BIT7      33
#define FIFO_BIT8      26
#define FIFO_BIT9      27
#define FIFO_BIT10     14
#define FIFO_BIT11     12


extern unsigned SampleSize;
extern unsigned SampleSizeFft;


void initFifo(void)
{
    pinMode(CHOOSE_CHANNEL, OUTPUT);
    pinMode(RCLK, OUTPUT);
    pinMode(WEN, OUTPUT);
    pinMode(RESET_FIFO, OUTPUT);
    digitalWrite(CHOOSE_CHANNEL, LOW);
    digitalWrite(RCLK, LOW);
    digitalWrite(WEN, HIGH);
    digitalWrite(RESET_FIFO, HIGH);

    pinMode(FIFO_BIT0, INPUT);
    pinMode(FIFO_BIT1, INPUT);
    pinMode(FIFO_BIT2, INPUT);
    pinMode(FIFO_BIT3, INPUT);
    pinMode(FIFO_BIT4, INPUT);
    pinMode(FIFO_BIT5, INPUT);
    pinMode(FIFO_BIT6, INPUT);
    pinMode(FIFO_BIT7, INPUT);
    pinMode(FIFO_BIT8, INPUT);
    pinMode(FIFO_BIT9, INPUT);
    pinMode(FIFO_BIT10, INPUT);
    pinMode(FIFO_BIT11, INPUT);
}


void measureCurrentAndVoltage(uint16_t *current, uint16_t *voltage, unsigned delaytime)
{
    fillFifo(delaytime);

    chooseCurrentChannel();
    readDataFromFifo(current, SampleSize);

    chooseVoltageChannel();
    readDataFromFifo(voltage, SampleSize);
}


void measureCurrent(uint16_t *current, unsigned delaytime)
{
    fillFifo(delaytime);

    chooseCurrentChannel();
    readDataFromFifo(current, SampleSizeFft);
}


void fillFifo(unsigned delaytime)
{
    // reset FIFO
    digitalWrite(RESET_FIFO, LOW); digitalWrite(RESET_FIFO, HIGH);
    delay(10);
    // write to FIFO
    digitalWrite(WEN, LOW); delay(delaytime); digitalWrite(WEN, HIGH);
    delay(2);
}


void readDataFromFifo(uint16_t *inputArray, unsigned size)
{
    delay(1);
    for (unsigned cntr = 0; cntr < size; ++cntr) {
        digitalWrite(RCLK, HIGH); // LOW if transistor is installed
        inputArray[cntr] = (float)readFifo();
        digitalWrite(RCLK, LOW); // HIGH if transistor is installed
    }
}


uint16_t readFifo(void)
{
    uint16_t fifodata = digitalRead(FIFO_BIT0) | (digitalRead(FIFO_BIT1) << 1) | (digitalRead(FIFO_BIT2) << 2) | (digitalRead(FIFO_BIT3) << 3) |
                 (digitalRead(FIFO_BIT4) << 4) | (digitalRead(FIFO_BIT5) << 5) | (digitalRead(FIFO_BIT6) << 6) | (digitalRead(FIFO_BIT7) << 7) |
                 (digitalRead(FIFO_BIT8) << 8) | (digitalRead(FIFO_BIT9) << 9) | (digitalRead(FIFO_BIT10) << 10) | (digitalRead(FIFO_BIT11) << 11);
    return fifodata;
}


void chooseCurrentChannel(void)
{
    digitalWrite(CHOOSE_CHANNEL, LOW);
}


void chooseVoltageChannel(void)
{
    digitalWrite(CHOOSE_CHANNEL, HIGH);
}
